//Solution: 

let num = 2;
const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

let address = ["258", "Washington Ave NW", "California", 90011];
let [block, street, state, zip] = address;
console.log(`I live at ${block} ${street}, ${state} ${zip}`);

let animal = {
    name: "Lolong",
    animalType: "saltwater crocodile",
    weight: 1075,
    height : {
        feet: 20,
        inches: 3
    }
}
let {name, animalType, weight, height, feet, inches} = animal;
console.log(`${name} was a ${animalType}. He weighed at ${weight} kgs with a 
measurement of ${height.feet} ft ${height.inches} in.`);

let arrayOfNumbers = [1, 2, 3, 4, 5];
arrayOfNumbers.forEach((number) => console.log(`${number}`));

let reduceNumber = arrayOfNumbers.reduce((acc,cur) => {
    return acc + cur;
})
console.log(reduceNumber);

class Dog{
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}
const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);